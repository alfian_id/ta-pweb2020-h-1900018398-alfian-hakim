<!DOCTYPE html>
<html>

<head>
    <title>Alfian's Web</title>
    <link rel="stylesheet" href="css/home_style.css?v=1.1">
</head>

<body onload="popup()">
    <div class="popup">
        <div class="popup-login">
            <h3 alt="close" class="close">&times;</h3>
            <h2>Login Tiketku.id</h2>
            <input class="ilogin" type="text" name="" placeholder="Username">
            <input class="ilogin" type="password" name="" placeholder="Password">
            <button type="submit" class="tombol" id="close-login">Login</button>
        </div>
    </div>

    <div class="container">
        <div class="header">
            <h1 class="judul" onclick="window.location.href='index.php'">Tiketku.id</h1>
            <hr class="garis">
            <p>Tiket Bus Jurusan Jakarta, Bandung, Semarang, Surabaya dan Bali</p>
        </div>
        <div class="menu">
            <ul>
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="public_html/pesan.html">Pesan</a></li>
                <li><a href="public_html/promo.html">Promo</a></li>
                <li><a href="public_html/buku.php">Buku</a></li>
                <li><a href="public_html/contact.html">Contact</a></li>
                <li><a href="public_html/kritik.php">Kritik</a></li>
            </ul>
            <div class="search">
                <input type="text" class="search-box" name="search" placeholder="Search Box">
            </div>
        </div>
        <div class="hero"></div>
        <div class="konten cf">
            <div class="utama">
                <h2>Selamat Datang di Web Agen Bus Kami !</h2>
                <p style="margin-bottom: 25px">Silahkan klik tombol dibawah ini untuk melakukan pemesanan</p>
                <input class="tombol" type="button" name="pesan" value="Pesan Tiket" style="padding: 11px 30px;" onclick="window.location.href='public_html/pesan.html'">
                <input class="tombol" type="button" name="pesan" value="Isi Buku Tamu" style="padding: 11px 25px;margin-left: 10px;" onclick="window.location.href='public_html/buku.php'">
                </div>
            <div class="sidebar">
                <h3>Jumlah Pengunjung :</h3>
                <div style="margin-top: -2px;margin-bottom: 10px;width: 260px;height: 3px;background: salmon;"></div>
                <?php 
                $filecounter=("data/pengunjung.txt");
                $kunjungan=file($filecounter);
                $kunjungan[0]++;
                $file=fopen($filecounter,"w");
                fputs($file,"$kunjungan[0]");
                fclose($file);

                echo "<div align=center style=border-style:solid;width:220px;height:35px;padding-top:5px;border-radius:10px;margin-left:20px;margin-top:10px;border-color:grey;><font style=font-weight:bold;letter-spacing:8px;font-size:25px>".$kunjungan[0];
                echo "</font></div>";
                ?>

                <h3 style="padding-top: 40px">Data Diri :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 260px;height: 3px;background: salmon;"></div>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;:&nbsp; Alfian Hakim</td>
                    </tr>
                    <tr>
                        <td>Nim</td>
                        <td>&nbsp;&nbsp;:&nbsp; 1900018398</td>
                    </tr>
                    <tr>
                        <td>Prodi</td>
                        <td>&nbsp;&nbsp;:&nbsp; Teknik Informatika</td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>&nbsp;&nbsp;:&nbsp; H</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer">
            <p class="cr">&copy; Copyright 2020.Alfian Hakim</p>
        </div>
    </div>
    <script src="js/script.js"></script>
</body>

</html>