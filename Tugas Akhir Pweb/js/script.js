function cari_tiket() {

	var nama = (document.fform.nama.value);
	var tgl = (document.fform.tgl.value);
	var tujuan = (document.fform.tujuan.value);
	var tiket = (document.fform.tiket.value);
	var member = (document.fform.member.value);
	var ht = 0.0;
	var sub = 0.0;
	var diskon = 0.0;
	var total = 0.0;


	if (nama == "" || tgl == "" || tiket == "") {
		alert("Isi Form Dengan Lengkap !");
	} else {
		document.documentElement.scrollTop = 1000;

		if (tujuan == "Jakarta") {
			ht = 190000;
		} else if (tujuan == "Bandung") {
			ht = 180000;
		} else if (tujuan == "Semarang") {
			ht = 80000;
		} else if (tujuan == "Surabaya") {
			ht = 150000;
		} else if (tujuan == "Bali") {
			ht = 200000;
		}
		sub = tiket * ht;

		if (member == "normal") {
			document.fform.odiskon.value = "0%";
			document.fform.ototal.value = "Rp." + sub;
		} else {
			diskon = 0.2 * sub;
			total = sub - diskon;
			document.fform.odiskon.value = "20% / Rp." + diskon;
			document.fform.ototal.value = "Rp." + total;
		}
		
		document.fform.otgl.value=tgl;
		document.fform.otujuan.value=tujuan;
		document.fform.otiket.value=tiket+" Tiket";
		document.fform.oht.value="Rp."+ht;
		document.fform.osub.value="Rp."+sub;

	}
}

function batalkan() {
	document.documentElement.scrollTop = 0;
}

function popup() {
	document.querySelector(".popup").style.display = "flex";
	document.querySelector(".close").addEventListener("click", function () {
		document.querySelector(".popup").style.display = "none"
	})
	document.querySelector("#close-login").addEventListener("click", function () {
		document.querySelector(".popup").style.display = "none"
	})
}

function popup2() {
	if (document.fform.osub.value=="") {
		alert("Data Kosong !!");
	}else{
	document.getElementById('tombol_bayar').addEventListener("click",function(){
		document.querySelector(".popup2").style.display = "flex";})
	document.querySelector(".close").addEventListener("click", function () {
		document.querySelector(".popup2").style.display = "none";
	})
	document.querySelector(".popup-bayar .tombol").addEventListener("click", function () {
		document.querySelector(".popup2").style.display = "none";
	})
	// window.scrollTo(0, 500);
	}
}
