<!DOCTYPE html>
<html>

<head>
    <title>Daftar Tamu</title>
    <link rel="stylesheet" href="../css/home_style.css">
</head>

<body>

    <div class="container">
        <div class="header">
            <h1 class="judul" onclick="window.location.href='../index.php'">Tiketku.id</h1>
            <hr class="garis">
            <p>Tiket Bus Jurusan Jakarta, Bandung, Semarang, Surabaya dan Bali</p>
        </div>
        <div class="menu">
            <ul>
                <li><a href="../index.php">Home</a></li>
                <li><a href="pesan.html">Pesan</a></li>
                <li><a href="promo.html">Promo</a></li>
                <li class="active"><a href="buku.php">Buku</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li><a href="kritik.php">Kritik</a></li>
            </ul>
            <div class="search">
                <input type="text" class="search-box" name="search" placeholder="Search Box">
            </div>
        </div>
        <div class="hero"></div>
        <div class="konten cf">
            <div class="utama">
                <h2>Daftar Tamu</h2>
                <p style="margin-bottom: 10px">Pengunjung dari Tiketku.id</p>
                <?php 
                    $fp= fopen("../data/bukutamu.txt","r");
                    echo "<table border=0><hr width=86% align=left>";
                    while($isi = fgets($fp,200)){

                        $pisah = explode("|",$isi);
                        echo "<tr><td>&nbsp&nbspNama </td><td>&nbsp : &nbsp$pisah[0]</td></tr>";
                        echo "<tr><td>&nbsp&nbspAlamat </td><td>&nbsp : &nbsp$pisah[1]</td></tr>";
                        echo "<tr><td>&nbsp&nbspE-mail </td><td>&nbsp : &nbsp$pisah[2]</td></tr>";
                        echo "<tr><td>&nbsp&nbspStatus </td><td>&nbsp : &nbsp$pisah[3]</td></tr>";
                        echo "<tr><td>&nbsp&nbspKomentar </td><td>&nbsp : &nbsp$pisah[4]</td></tr>";
                        echo "<tr><td>&nbsp&nbspDisubmit Pada </td><td>&nbsp : &nbsp$pisah[5]</td></tr>
                        <tr><td colspan=2><hr width=130%></td></tr>";
                    }
                    echo "</table>";
                 ?>
                 <input class="tombol" type="button" name="pesan" value="Isi Buku Tamu" style="padding: 9px 35px;margin-top: 20px;" onclick="window.location.href='buku.php'">
            </div>
            <div class="sidebar">
                <h3>Data Diri :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 270px;height: 3px;background: salmon;"></div>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;:&nbsp; Alfian Hakim</td>
                    </tr>
                    <tr>
                        <td>Nim</td>
                        <td>&nbsp;&nbsp;:&nbsp; 1900018398</td>
                    </tr>
                    <tr>
                        <td>Prodi</td>
                        <td>&nbsp;&nbsp;:&nbsp; Teknik Informatika</td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>&nbsp;&nbsp;:&nbsp; H</td>
                    </tr>
                </table>
                <h3 style="margin-top: 30px">IP Address :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 270px;height: 3px;background: salmon;"></div>
                <?php 
                    function ip_address() {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                        $ipaddress = getenv('HTTP_CLIENT_IP');
                    else if(getenv('HTTP_X_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                    else if(getenv('HTTP_X_FORWARDED'))
                        $ipaddress = getenv('HTTP_X_FORWARDED');
                    else if(getenv('HTTP_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    else if(getenv('HTTP_FORWARDED'))
                       $ipaddress = getenv('HTTP_FORWARDED');
                    else if(getenv('REMOTE_ADDR'))
                        $ipaddress = getenv('REMOTE_ADDR');
                    else
                        $ipaddress = 'IP tidak dikenali';
                    return $ipaddress;
                }

                    echo "<div align=center style=border-style:solid;width:260px;height:35px;padding-top:5px;border-radius:10px;margin-top:10px;border-color:grey;><font style=font-weight:bold;font-size:20px>".ip_address();
                echo "</font></div>";
                 ?>
            </div>
        </div>
        <div class="footer">
            <p class="cr">&copy; Copyright 2020.Alfian Hakim</p>
        </div>
    </div>

</body>

</html>