<!DOCTYPE html>
<html>

<head>
    <title>Buku Tamu</title>
    <link rel="stylesheet" href="../css/home_style.css">
</head>

<body>
    <div class="container">
        <div class="header">
            <h1 class="judul" onclick="window.location.href='../index.php'">Tiketku.id</h1>
            <hr class="garis">
            <p>Tiket Bus Jurusan Jakarta, Bandung, Semarang, Surabaya dan Bali</p>
        </div>
        <div class="menu">
            <ul>
                <li><a href="../index.php">Home</a></li>
                <li><a href="pesan.html">Pesan</a></li>
                <li><a href="promo.html">Promo</a></li>
                <li class="active"><a href="buku.php">Buku</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li><a href="kritik.php">Kritik</a></li>
            </ul>
            <div class="search">
                <input type="text" class="search-box" name="search" placeholder="Search Box">
            </div>
        </div>
        <div class="hero"></div>
        <div class="konten cf">
            <div class="utama">
                <h2>Buku Tamu</h2>
                <p style="margin-bottom: 25px">Harap isi form buku tamu dibawah</p>
                <form action="buku.php" method="post" style="margin-top: 20px;">
            <div class="fform" align="left">
                <label>Nama Lengkap :</label><br>
                <input class="input-box" type="text" name="nama" id="nama" placeholder="Masukan nama anda" required oninvalid="this.setCustomValidity('Harap Masukan Nama')" oninput="setCustomValidity('')"><br>
                <label>Alamat :</label><br>
                <input class="input-box" type="text" name="alamat" id="alamat" placeholder="Masukan alamat anda" required oninvalid="this.setCustomValidity('Harap Masukan Alamat')" oninput="setCustomValidity('')"><br>
                <label>E-Mail :</label><br>
                <input class="input-box" type="text" name="email" id="email" placeholder="Masukan E-Mail" required oninvalid="this.setCustomValidity('Harap Masukan E-Mail')" oninput="setCustomValidity('')"><br>
                <label>Status :</label><br>
                <select class="input-box" name="status" id="status">
                    <option>-Pilih-</option>
                    <option>Menikah</option>
                    <option>Lajang</option>
                    <option>Mahasiswa</option>
                    <option>Pelajar</option>
                </select><br>
                <label>Komentar :</label><br>
                <input class="input-box" type="text" name="komentar" id="komentar" placeholder="Masukan Komentar Anda" style="height: 70px;margin-bottom: 20px" required oninvalid="this.setCustomValidity('Harap Masukan Komentar Anda')" oninput="setCustomValidity('')"><br>
            </div>
                <button type="submit" class="tombol2" name="submit" style="margin: 0px 15px 0px 5px;">Submit</button>
                <button type="reset" class="tombol" style="padding: 10px 35px;">Reset</button><br>
                <input class="tombol" type="button" name="pesan" value="Daftar Tamu Tiketku.id" style="padding: 9px 45px;margin: 30px 0px 0px 110px;" onclick="window.location.href='daftartamu.php'">

                <?php 
                if (isset($_POST["submit"])) {
                date_default_timezone_set('Asia/Jakarta');
                function hari_ini(){
                    $hari = date ("D");
 
                switch($hari){
                    case 'Sun':
                        $hari_ini = "Minggu";
                    break;
 
                    case 'Mon':         
                        $hari_ini = "Senin";
                    break;
 
                    case 'Tue':
                        $hari_ini = "Selasa";
                    break;
 
                    case 'Wed':
                        $hari_ini = "Rabu";
                    break;
 
                    case 'Thu':
                        $hari_ini = "Kamis";
                    break;
 
                    case 'Fri':
                        $hari_ini = "Jumat";
                    break;
 
                    case 'Sat':
                        $hari_ini = "Sabtu";
                    break;
        
                    default:
                        $hari_ini = "Tidak di ketahui";     
                    break;
                    }
                        return $hari_ini;
                }

                $fp = fopen("../data/bukutamu.txt","a+");
                $nama = $_POST['nama'];
                $alamat = $_POST['alamat'];
                $email = $_POST['email'];
                $status = $_POST['status'];
                $komentar = $_POST['komentar'];
                $hari = hari_ini();
                $tanggal = date(", d/m/Y g:i:s a");

                fputs($fp,"$nama|$email|$alamat|$status|$komentar|$hari $tanggal\n");
                fclose($fp);
                echo "<script type='text/javascript'>alert('Terima Kasih, Tanggapan Anda Berhasil Direkam')</script>";
            }
                ?>
        </form>
            </div>
            <div class="sidebar">
                <h3>Data Diri :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 270px;height: 3px;background: salmon;"></div>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;:&nbsp; Alfian Hakim</td>
                    </tr>
                    <tr>
                        <td>Nim</td>
                        <td>&nbsp;&nbsp;:&nbsp; 1900018398</td>
                    </tr>
                    <tr>
                        <td>Prodi</td>
                        <td>&nbsp;&nbsp;:&nbsp; Teknik Informatika</td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>&nbsp;&nbsp;:&nbsp; H</td>
                    </tr>
                </table>
                <h3 style="margin-top: 30px">IP Address :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 270px;height: 3px;background: salmon;"></div>
                <?php 
                    function ip_address() {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                        $ipaddress = getenv('HTTP_CLIENT_IP');
                    else if(getenv('HTTP_X_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                    else if(getenv('HTTP_X_FORWARDED'))
                        $ipaddress = getenv('HTTP_X_FORWARDED');
                    else if(getenv('HTTP_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    else if(getenv('HTTP_FORWARDED'))
                       $ipaddress = getenv('HTTP_FORWARDED');
                    else if(getenv('REMOTE_ADDR'))
                        $ipaddress = getenv('REMOTE_ADDR');
                    else
                        $ipaddress = 'IP tidak dikenali';
                    return $ipaddress;
                }

                    echo "<div align=center style=border-style:solid;width:260px;height:35px;padding-top:5px;border-radius:10px;margin-top:10px;border-color:grey;><font style=font-weight:bold;font-size:20px>".ip_address();
                echo "</font></div>";
                 ?>
            </div>
        </div>
        <div class="footer">
            <p class="cr">&copy; Copyright 2020.Alfian Hakim</p>
        </div>
    </div>
<script src="../js/script.js"></script>
</body>
</html>