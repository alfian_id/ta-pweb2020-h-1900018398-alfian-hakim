<!DOCTYPE html>
<html>

<head>
    <title>Kritik dan Saran</title>
    <link rel="stylesheet" href="../css/home_style.css">
</head>

<body>

    <div class="container">
        <div class="header">
            <h1 class="judul" onclick="window.location.href='../index.php'">Tiketku.id</h1>
            <hr class="garis">
            <p>Tiket Bus Jurusan Jakarta, Bandung, Semarang, Surabaya dan Bali</p>
        </div>
        <div class="menu">
            <ul>
                <li><a href="../index.php">Home</a></li>
                <li><a href="pesan.html">Pesan</a></li>
                <li><a href="promo.html">Promo</a></li>
                <li><a href="buku.php">Buku</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li class="active"><a href="kritik.php">Kritik</a></li>
            </ul>
            <div class="search">
                <input type="text" class="search-box" name="search" placeholder="Search Box">
            </div>
        </div>
        <div class="hero"></div>
        <div class="konten cf">
            <div class="utama">
                <h2>Kritik Dan Saran</h2>
                <p style="margin-bottom: 20px">Kritik dan saran dari anda akan sangat berguna untuk kami</p>
                <form action="kritik.php" method="post" style="margin-top: 20px;">
                <div class="fform" align="left">
                <label>Nama :</label><br>
                <input class="input-box" type="text" name="nama" id="nama" placeholder="Masukan nama anda" required oninvalid="this.setCustomValidity('Harap Masukan Nama')" oninput="setCustomValidity('')"><br>
                <label>Komentar :</label><br>
                <input class="input-box" type="text" name="komentar" id="komentar" placeholder="Masukan Komentar Anda" style="height: 100px;margin-bottom: 20px;padding-bottom: 70px;" required oninvalid="this.setCustomValidity('Harap Masukan Komentar')" oninput="setCustomValidity('')"><br>
            </div>
                <button type="submit" class="tombol2" name="submit" style="margin: 0px 20px 0px 5px;">Submit</button>
                <button type="reset" class="tombol">Reset</button>
            </form>
            <?php 
            if (isset($_POST["submit"])) {
            date_default_timezone_set('Asia/Jakarta');
            function hari_ini(){
                $hari = date ("D");
 
                switch($hari){
                case 'Sun':
                    $hari_ini = "Minggu";
                break;
 
                case 'Mon':         
                    $hari_ini = "Senin";
                break;
 
                case 'Tue':
                    $hari_ini = "Selasa";
                break;
 
                case 'Wed':
                    $hari_ini = "Rabu";
                break;
 
                case 'Thu':
                    $hari_ini = "Kamis";
                break;
 
                case 'Fri':
                    $hari_ini = "Jumat";
                break;
 
                case 'Sat':
                    $hari_ini = "Sabtu";
                break;
        
                default:
                    $hari_ini = "Tidak di ketahui";     
                break;
            }
 
                return $hari_ini;
 
            }

            function ip_address() {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                        $ipaddress = getenv('HTTP_CLIENT_IP');
                    else if(getenv('HTTP_X_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                    else if(getenv('HTTP_X_FORWARDED'))
                        $ipaddress = getenv('HTTP_X_FORWARDED');
                    else if(getenv('HTTP_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    else if(getenv('HTTP_FORWARDED'))
                       $ipaddress = getenv('HTTP_FORWARDED');
                    else if(getenv('REMOTE_ADDR'))
                        $ipaddress = getenv('REMOTE_ADDR');
                    else
                        $ipaddress = 'IP tidak dikenali';
                    return $ipaddress;
                }

            function browser() {
                $browser = '';
                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape'))
                        $browser = 'Netscape';
                    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox'))
                        $browser = 'Firefox';
                    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome'))
                        $browser = 'Google Chrome';
                    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Opera'))
                        $browser = 'Opera';
                    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
                        $browser = 'Internet Explorer';
                    else
                     $browser = 'Other';
                    return $browser;
                }

                $fp = fopen("../data/kritik.txt","a+");
                $nama = $_POST['nama'];
                $komentar = $_POST['komentar'];
                $hari = hari_ini();
                $tanggal = date(", d/m/Y g:i:s a");
                $ip = ip_address();
                $browser = browser();
                   
                fputs($fp,"Nama       : $nama\nKomentar   : $komentar\nIp Address : $ip\nBrowser    : $browser\nDisubmit   : $hari$tanggal\n-------------------------------------------------\n");
                fclose($fp);
                echo "<script type='text/javascript'>alert('Terima Kasih, Tanggapan Anda Berhasil Direkam')</script>";
            }
             ?> 
            </div>
            <div class="sidebar">
                <h3>Data Diri :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 290px;height: 3px;background: salmon;"></div>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>&nbsp;&nbsp;:&nbsp; Alfian Hakim</td>
                    </tr>
                    <tr>
                        <td>Nim</td>
                        <td>&nbsp;&nbsp;:&nbsp; 1900018398</td>
                    </tr>
                    <tr>
                        <td>Prodi</td>
                        <td>&nbsp;&nbsp;:&nbsp; Teknik Informatika</td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>&nbsp;&nbsp;:&nbsp; H</td>
                    </tr>
                </table>
               <!--  <h3 style="margin-top: 30px">Kesan dan Pesan :</h3>
                <div style="margin-top: -3px;margin-bottom: 10px;width: 290px;height: 3px;background: salmon;"></div>
                <p align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Walaupun praktikum semester 2 ini dilakukan secara online,tetapi saya tidak ada kesulitan karena asprak sudah memberikan source code dan langkah-langkah praktikum dengan jelas pada video ,, semoga kedepannya bisa terus menjelaskan praktikum dengan jelas ,mungkin asprak ingin memberikan kritik dan saran silahkan mengisi form di samping :) ,, Terima Kasih</p> -->
            </div>
        </div>
        <div class="footer">
            <p class="cr">&copy; Copyright 2020.Alfian Hakim</p>
        </div>
    </div>

</body>

</html>